import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;

public class Test1 extends AbstractTest {
    @Test
    void vAuthorization1() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"oehrw7")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"71fa54997c")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertTrue( getDriver().findElement(cssSelector(".hero-section > .svelte-1e9zcmy")).getText().contains("Blog"));
    }
    @Test
    void vAuthorization2() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"txl")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"30299f2131")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertTrue( getDriver().findElement(cssSelector(".hero-section > .svelte-1e9zcmy")).getText().contains("Blog"));
    }
    @Test
    void vAuthorization3() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"20202020202020202020")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"b1b43081b4")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertTrue( getDriver().findElement(cssSelector(".hero-section > .svelte-1e9zcmy")).getText().contains("Blog"));
    }
    @Test
    void nvAuthorization1() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"ew")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"79a628b2d9")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertFalse( getDriver().findElement(cssSelector(".hero-section > .svelte-1e9zcmy")).getText().contains("Blog"));
    }
    @Test
    void nvAuthorization2() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"212121212121212121212")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"c5bbbd38ad")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertFalse( getDriver().findElement(cssSelector(".hero-section > .svelte-1e9zcmy")).getText().contains("Blog"));
    }
    @Test
    void nvAuthorization3() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"wgehi@")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"92df29f614")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertFalse( getDriver().findElement(cssSelector(".hero-section > .svelte-1e9zcmy")).getText().contains("Blog"));
    }
    @Test
    void nvAuthorization4() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"71fa54997c")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertTrue( getDriver().findElement(cssSelector(".error-block > .svelte-uwkxn9:nth-child(1)")).getText().contains("401"));
    }
    @Test
    void nvAuthorization5() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"oehrw7")
                .pause(1000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertTrue( getDriver().findElement(cssSelector(".error-block > .svelte-uwkxn9:nth-child(1)")).getText().contains("401"));
    }
    @Test
    void nvAuthorization6() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"hiogjv")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"gwrijo83")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(2000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertTrue( getDriver().findElement(cssSelector(".error-block > .svelte-uwkxn9:nth-child(1)")).getText().contains("401"));
    }
    @Test
    void viewPost() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"oehrw7")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"71fa54997c")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(10000l)
                .click(getDriver().findElement(By.xpath("/html/body/div[1]/main/div/div[3]/div[1]/a[1]/h2")))
                .pause(3000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertTrue(getDriver().findElement(cssSelector(".container:nth-child(1) > .svelte-tv8alb")).getText().contains("BOB THE PHILANTROPIST"));
    }
    @Test
    void viewNextPage() throws InterruptedException {
        Actions autho = new Actions(getDriver());
        WebElement element1 = getDriver().findElement(xpath("//form[@id='login']/div/label/input"));
        element1.click();
        autho
                .sendKeys(getDriver().findElement(xpath("//form[@id='login']/div/label/input")),"oehrw7")
                .pause(1000l)
                .click(getDriver().findElement(xpath("//input[@type='password']")))
                .pause(1000l)
                .sendKeys(getDriver().findElement(xpath("//input[@type='password']")),"71fa54997c")
                .pause(2000l)
                .click(getDriver().findElement(By.cssSelector(".mdc-button__ripple")))
                .pause(1000l)
                .click(getDriver().findElement(By.xpath("//a[contains(text(),'Next Page')]")))
                .pause(1000l)
                .build()
                .perform();
        Thread.sleep(1000);
        Assertions.assertTrue(getDriver().findElement(cssSelector(".svelte-127jg4t:nth-child(2)")).getText().contains("oyuhtw"));
    }
}
